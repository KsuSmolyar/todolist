const list = document.querySelector(".list"); //Получили нумерованный список, куда мы будем записываать задачи
const btnSave = document.querySelector(".btn"); //Получили кнопку "Сохранить", по нажатию на которую введенная задача добавляется в список
const inp = document.querySelector(".inp"); //Получили поле ввода задачи
const btnMoving = document.querySelector("#moving"); //Получаем кнопку "добавить в выполненное"
const btnRemoveMultiply = document.querySelector("#remove"); //получаем кнопку "удалить"
const btnContainer = document.querySelector(".controls-container"); //Получаем контейнер с кнопками добавить в выполненное и удалить
const btnCompleted = document.querySelector("#completed"); //Получаем кнопку "выбранное"
const mainTitle = document.querySelector(".main-title");

let toDos = []; //Инициализировали пустой массив для хранения объектов заданных задач
let toDosCompleted = []; //Инициализировали пустой массив для хранения объектов выполненных задач

//Повешали обаботчик событий на документ, который срабатывает при ззагрузке страницы
document.addEventListener("DOMContentLoaded", () => {
	toDos = getFromLocalStorage("toDos"); //запускаем функию получить из локал сторэдж
	initToDos(toDos);
	if (
		btnContainer.classList.contains("show") &&
		!toDos.some((toDo) => toDo.complete)
	) {
		btnContainer.classList.remove("show");
	} else if (toDos.some((toDo) => toDo.complete)) {
		btnContainer.classList.add("show");
	}
});

// 1 Функия создания элемента списка  срабатывает при загрузке страницы

function initToDos(arr) {
	//мапим массив toDos по элементам и для каждого элемента возвращаем вызов функции создать элемент, все это получаем в новый массив
	const toDoElements = arr.map((toDo) => {
		return createEl(toDo.complete, toDo.title, toDo.id);
	});
	if (list.hasChildNodes()) {
		list.innerHTML = "";
	}
	list.append(...toDoElements); //добавлем внуть элемента лист наш массив с ипользованием спредоператора
}

// 2 Получаем из LS
function getFromLocalStorage(key) {
	const localS = JSON.parse(localStorage.getItem(key)); //получаем в переменную массив из LS
	//возвращаем localS
	return localS != null ? localS : [];
}

// 3 Функция, срабатывающая при нажатии на кнопку "сохранить"

function saveToDos() {
	if (inp.value.trim() != "") {
		const toDo = createToDo(); //Создаем константу, в нее присваиваем и вызываем функцию создания туду списка задач (вернет объект)
		toDos.push(toDo); //пушим в массив наш объект с задачей
		setToLocalStorage("toDos", toDos); //Вызываем функцию, которая сохранит наш массив в локал сторедж
		list.append(createEl(toDo.complete, toDo.title, toDo.id)); //добавляем в элемент лист(список задач), вызывая функцию создать элемент(разметку элемента)
	}
	inp.value = ""; //Обнуляем поле ввода
}

// 4 Создаем объект, который запишем  массив как элемент списка
function createToDo() {
	return {
		id: Date.now(), //уникальный id
		complete: false, //завершенность
		title: inp.value, //Заголовок задачи  - значение из инпута
	};
}

// 5 Записываем в локал сторэдж

function setToLocalStorage(key, arr) {
	localStorage.setItem(key, JSON.stringify(arr)); //По колючу toDos записываем массив, преобразованный в джейсон формат
}

// 6 Создаем элемент
function createEl(complete, title, id) {
	const item = document.createElement("li"); //создаем элемент li
	item.className = "list__item"; // добавляем элементу ккласс
	item.dataset.id = id; //добавляем элементу id
	item.draggable = true;
	//Добавляем в элемент разметку(используем бэктики), значения потом возьмем из созданного объекта
	item.innerHTML = `<div class="item-wrap">
 <input type="checkbox" class="inp-checkbox" ${
		complete ? "checked" : ""
 }> <p class="list__text">${title}</p> <button class="btn_delete"> <img class="delete-icon" src="trashcan.svg"/> </button>
 </div>`;
	const btnDelete = item.querySelector(".btn_delete"); //Создаем кнопку удаления задачи из списка
	btnDelete.addEventListener("click", (event) => {
		deleteElFromList(event.currentTarget.closest(".list__item"));
	}); //Навешиваем обработчик собтий на кнопку удаления, передаем функцию deleteEl
	//функция createEl возвращает элемент
	const inpCheckbox = item.querySelector(".inp-checkbox");
	inpCheckbox.addEventListener("change", changeComplete);
	return item;
}

//8 Фуункция удалить элемент из списка задач, вызывается в ф-и createEl

function deleteElFromList(delEl) {
	const elId = delEl.dataset.id;
	toDos = toDos.filter((el) => el.id != elId);
	setToLocalStorage("toDos", toDos);
	delEl.remove();
}

function changeComplete(event) {
	const checkedEl = event.currentTarget.closest(".list__item"); //Получаем элемент с выбранным чекбоксом
	const elId = checkedEl.dataset.id; //Получаем id выбранного элемента

	//Мапим массив toDos и если у элемента массива(объекта) id равен idEl, то для элемента свойство complete равно свойству checked от чекбокса(тру или фолс)
	toDos = toDos.map((toDo) => {
		if (toDo.id == elId) {
			toDo.complete = event.currentTarget.checked;
		}
		return toDo;
	});

	if (
		btnContainer.classList.contains("show") &&
		!toDos.some((toDo) => toDo.complete)
	) {
		btnContainer.classList.remove("show");
	} else if (toDos.some((toDo) => toDo.complete)) {
		btnContainer.classList.add("show");
	}
	setToLocalStorage("toDos", toDos);
}

// 9 По нажатию на кнопку запускаем функцию сохранить тудус(массив)
btnSave.addEventListener("click", saveToDos);

list.addEventListener("dragstart", (event) => {
	event.target.classList.add("selected");
});

list.addEventListener("dragend", (event) => {
	event.target.classList.remove("selected");
	setToLocalStorage("toDos", toDos);
});

list.addEventListener("dragover", (event) => {
	event.preventDefault(); //Разрешаем сбрасывать элемент в выбранную область

	//Находим перемещаемый элемент
	const activeEl = list.querySelector(".selected");

	//Находим элемент, над которым в данный момент находится курсор
	const currentEl = event.target.closest(".list__item");
	//Проверяем, что событие сработало: 1. не на том элементе, который мы перемещаем, 2. именно на элементе списка
	const isMoveable =
		currentEl != null &&
		activeEl !== currentEl &&
		currentEl.classList.contains("list__item");

	//Если нет, прерываем выполнение функции
	if (!isMoveable) {
		return;
	}

	//Находим элемент, перед которым будем вставлять
	const nextElement =
		currentEl === activeEl.nextElementSibling
			? currentEl.nextElementSibling
			: currentEl;
	//Вставляем activeEl перед nextEl
	list.insertBefore(activeEl, nextElement);

	let activeElId = activeEl.dataset.id; //Получаем id у activeEl
	let currentElId = currentEl.dataset.id; //Получаем id у curreentEl

	const activeElIndex = toDos.findIndex((toDo) => toDo.id == activeElId);

	const currentElIndex = toDos.findIndex((toDo) => toDo.id == currentElId);
	swap(toDos, activeElIndex, currentElIndex);
});

function swap(arr, a, b) {
	arr[a] = arr.splice(b, 1, arr[a])[0];
}

//По нажатию на кнопку добавить в выполненное фильтруем в массив toDosCompleted из массива toDos по свойству complete: true
btnMoving.addEventListener("click", () => {
	toDosCompleted = [
		...toDosCompleted,
		...toDos.filter((toDo) => toDo.complete === true),
	];

	setToLocalStorage("toDosCompleted", toDosCompleted); //Сохраняем массив toDosCompleted в LS
	//Переираем массив toDosCompleted методом forEach, получаем элемент с атрибутом data и вызываем для этого элемента функцию deleteElFromList
	toDosCompleted.forEach((toDo) => {
		const delEl = list.querySelector(`[data-id="${toDo.id}"]`);
		delEl && deleteElFromList(delEl);
	});
});

btnCompleted.addEventListener("click", () => {
	if (btnContainer.classList.contains("show")) {
		btnContainer.classList.remove("show");
	}
	if (btnCompleted.textContent == "Выполненное") {
		toDosCompleted = getFromLocalStorage("toDosCompleted");
		initToDos(toDosCompleted);
		btnCompleted.textContent = "Вернуться к списку задач";
		btnSave.setAttribute("disabled", true);
		inp.setAttribute("disabled", true);
		mainTitle.textContent = "Выполненные задачи";
	} else if (btnCompleted.textContent == "Вернуться к списку задач") {
		toDos = getFromLocalStorage("toDos");
		initToDos(toDos);
		btnCompleted.textContent = "Выполненное";
		btnSave.removeAttribute("disabled");
		inp.removeAttribute("disabled");
		mainTitle.textContent = "Список задач";
	}
});

inp.addEventListener("keyup", (event) => {
	if (event.keyCode == 13) {
		saveToDos();
	}
});
